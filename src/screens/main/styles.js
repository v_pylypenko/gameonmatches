import {StyleSheet, Dimensions} from 'react-native';

const WIDTH = Dimensions.get('window').width;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  pileStyle: {
    width: WIDTH,
    height: 300,
  },
  btnContainer: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  touchableStyle: {
    marginVertical: 5,
    padding: 10,
    backgroundColor: '#800080',
  },
  whiteFont: {
    color: '#FFF',
  },
  mainFont: {
    fontSize: 24,
  },
  matchStyle: {
    width: 15,
    height: 30,
  },
  scoreboard: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default styles;

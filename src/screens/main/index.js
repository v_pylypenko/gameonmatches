import React, {useState} from 'react';
import {View, Image, Text, TouchableOpacity, Alert} from 'react-native';
import s from './styles';
import matches from '../../../accets/matches.jpg';
import match from '../../../accets/match.jpg';

const Matches = () => {
  const N = 25;
  const m = [1, 2, 3];
  const [totalCountMathes, setTotal] = useState(N);
  const [totalCountUserMatches, setUresMatches] = useState(0);
  const [listButtons, setListButtons] = useState(false);
  const [totalCountIIMatches, setTotalCountIIMatches] = useState(0);

  const IIMove = () => {
    let res = 0;
    let mod = totalCountMathes % 4;
    console.log('II, now: ', totalCountMathes);
    if (totalCountMathes === 3) {
      res = 1;
    } else if (totalCountMathes < 2) {
      console.log('res = 0', res);
      return res;
    } else if (mod === 1 || mod === 0) {
      res = 1;
    } else {
      res = 3;
    }
    setTotalCountIIMatches(totalCountIIMatches + res);
    console.log('II choose: ', res);
    return res;
  };
  const userTurn = (countMatches) => {
    setUresMatches(totalCountUserMatches + countMatches);
  };

  return (
    <View style={s.container}>
      <View style={s.scoreboard}>
        <Text style={s.mainFont}>В кучке {totalCountMathes}</Text>
        <Image source={match} style={s.matchStyle} />
      </View>
      <TouchableOpacity
        onPress={() => setListButtons(!listButtons)}
        disabled={listButtons}>
        <Image source={matches} style={s.pileStyle} />
      </TouchableOpacity>
      {listButtons && (
        <View style={s.btnContainer}>
          {m.map((el, index) => (
            <TouchableOpacity
              key={index}
              style={s.touchableStyle}
              onPress={() => {
                userTurn(el);
                setListButtons(!listButtons);
                let IIMathches = IIMove();
                setTotal(totalCountMathes - (el + IIMathches));
                setTotalCountIIMatches(totalCountIIMatches + IIMathches);
              }}>
              <Text style={s.whiteFont}>Вынуть {el}</Text>
            </TouchableOpacity>
          ))}
        </View>
      )}
      <View style={s.scoreboard}>
        <Text style={s.mainFont}>У тебя сейчас: {totalCountUserMatches}</Text>
        <Image source={match} style={s.matchStyle} />
      </View>
      {totalCountMathes === 0 &&
        totalCountIIMatches % 2 === 0 &&
        Alert.alert(
          'Message',
          'Ты проиграл, попробуй снова',
          [
            {
              text: 'Cancel',
              onPress: () => console.log('Cancel'),
              style: 'cancel',
            },
            {
              text: 'OK',
              onPress: () => {
                setTotal(N);
                setUresMatches(0);
                setTotalCountIIMatches(0);
              },
            },
          ],
          {cancelable: false},
        )}
      {totalCountUserMatches % 2 === 0 &&
        totalCountMathes === 0 &&
        Alert.alert('Я проиграл', 'Давай сразимся снова!!!', [
          {
            text: 'OK',
            onPress: () => {
              setTotal(N);
              setUresMatches(0);
              setTotalCountIIMatches(0);
            },
          },
        ])}
    </View>
  );
};
export default Matches;
